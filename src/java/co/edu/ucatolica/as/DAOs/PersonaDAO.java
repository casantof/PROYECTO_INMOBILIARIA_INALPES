/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucatolica.as.DAOs;

import co.edu.ucatolica.as.DTOs.Persona;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nixoduaa
 */
public class PersonaDAO {
    
    
    
    public boolean crearPersona(Persona p, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando crearPersona...");
            
            pstmt = con.prepareStatement("INSERT INTO persona ( IdPersona,Identificacion,Nombre_1,Nombre_2, "
                    + " Apellido_1, Apellido_2, Genero, Telefono, Email, "
                    + " Fecha_nacimiento, Tipo_Persona_idTipo_Persona) "
                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?)");
            
           
            pstmt.setInt(1, p.getId());
            pstmt.setString(2, p.getIdentificacion());
            pstmt.setString(3, p.getNombre1());
            pstmt.setString(4, p.getNombre2());
            pstmt.setString(5, p.getApellido1());
            pstmt.setString(6, p.getApellido2());
            pstmt.setString(7, p.getGenero());
            pstmt.setString(8, p.getTelef());
            pstmt.setString(9, p.getEmail());
            pstmt.setString(10, p.getfNacimiento());
            pstmt.setString(11, p.getTipoP());
            
            pstmt.execute();
            
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }

    public ArrayList<Persona> consultarPersona(Persona p, Connection con)
    {
        
        ArrayList<Persona> datos = new ArrayList();
        
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
        
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select IdPersona,Identificacion,Nombre_1,Nombre_2, "
                    + " Apellido_1, Apellido_2, Genero, Telefono, Email, " 
                    + " Fecha_nacimiento, Tipo_Persona_idTipo_Persona "
                    + " from persona "
                    + " where "
                    + " Identificacion='" + p.getIdentificacion()+"'"
                    + " AND Nombre_1='"+p.getNombre1()+"'");
            
            while (rs.next())
            { 
                Persona per = new Persona();
                per.setId(rs.getInt(1));
                per.setIdentificacion(rs.getString(2));
                per.setNombre1(rs.getString(3));
                per.setNombre2(rs.getString(4));
                per.setApellido1(rs.getString(5));
                per.setApellido2(rs.getString(6));
                per.setGenero(rs.getString(7));
                per.setTipoP(rs.getString(11));
                per.setfNacimiento(rs.getString(10));
                per.setTelef(rs.getString(8));
                per.setEmail(rs.getString(9));
                
                
                
                datos.add(per);
                
            }
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona fin..." + datos.size());
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    public ArrayList<Persona> consultarPersonas(Connection con)
    {
        
        ArrayList<Persona> datos = new ArrayList();
        
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
        
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select IdPersona,Identificacion,Nombre_1,Nombre_2, "
                    + " Apellido_1, Apellido_2, Genero, Telefono, Email, " 
                    + " Fecha_nacimiento, Tipo_Persona_idTipo_Persona "
                    + " from persona ");
            
            while (rs.next())
            { 
                Persona per = new Persona();
                per.setId(rs.getInt(1));
                per.setIdentificacion(rs.getString(2));
                per.setNombre1(rs.getString(3));
                per.setNombre2(rs.getString(4));
                per.setApellido1(rs.getString(5));
                per.setApellido2(rs.getString(6));
                per.setGenero(rs.getString(7));
                per.setTipoP(rs.getString(11));
                per.setfNacimiento(rs.getString(10));
                per.setTelef(rs.getString(8));
                per.setEmail(rs.getString(9));

                datos.add(per);
                
            }
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona fin..." + datos.size());
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    
    
    
         
    public boolean editarPersona(Persona p, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando editarPersona...");
            
            pstmt = con.prepareStatement("UPDATE persona "
                    + " SET "
                    + "IdPersona=?"
                    + ",Nombre_1=?"
                    + ",Nombre_2=?"
                    + ",Apellido_1=?"
                    + ",Apellido_2=?"
                    + ",Genero=?"
                    + ",Telefono=?"
                    + ",Email=?" 
                    + ",Fecha_nacimiento=?"
                    + ",Tipo_Persona_idTipo_Persona=?"
                    + " where Identificacion=?");
            
            pstmt.setInt(1, p.getId());
            pstmt.setString(11, p.getIdentificacion());
            pstmt.setString(2, p.getNombre1());
            pstmt.setString(3, p.getNombre2());
            pstmt.setString(4, p.getApellido1());
            pstmt.setString(5, p.getApellido2());
            pstmt.setString(6, p.getGenero());
            pstmt.setString(10, p.getTipoP());
            pstmt.setString(9, p.getfNacimiento());
            pstmt.setString(7, p.getTelef());
            pstmt.setString(8, p.getEmail());
            pstmt.executeUpdate();            
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }
}
